var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var config = require('../config/main')[process.env.NODE_ENV || 'development'];
var awsEndpoint = config.aws.s3Endpoint + '/';
var awsBucket = config.aws.imageBucketName;

var UserSchema = new mongoose.Schema({

    role: {
        type: String,
        enum: ['Resident', 'Admin', 'Super Admin'],
        default: 'Resident'
    },

    name: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    },

    contactEmail: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },

    contactPhone: {
        type: String
    },

    contactPhoneAlter: {
        type: String
    },

    block: {
        type: String,
        default: ''
    },

    flatNo: {
        type: String,
        default: ''
    },

    flatArea: String,

    profession: {
        type: String,
        default: 'Not Mentioned'
    },

    organization: {
        type: String,
        default: ''
    },

    designation: {
        type: String,
        default: ''
    },

    carNumber1: {
        type: String,
        default: ''
    },
    parkingSpace1: {
        type: String,
        default: ''
    },
    carNumber2: {
        type: String,
        default: ''

    },
    parkingSpace2: {
        type: String,
        default: ''
    },

    carNumber3: {
        type: String,
        default: ''
    },
    parkingSpace3: {
        type: String,
        default: ''
    },
    profilePicURL: {
        type: String,
        default: ''
    },

    profilePicKey: {
        type: String,
        default: ''
    },

    isProfilePrivate: {
        type: Boolean,
        default: false,

    },


    residentType: {
      type: String,
      default: function () {
        if (this.role == 'Resident') {
          return "Registered Owner";
        }
        else {
          return null;
        }
      }
    },
    isResiding: {
      type: Boolean,
      default: true
    },
    intercom: {
      type: String
    },


    myServices: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'ServiceOffered'
    }],

    societyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Society',
        required: function(){
            if (this.role == 'Resident' || this.role == 'Admin' )
                return true;
            else if (this.role == 'Super Admin')
                return false;
        }
    },

    softDeleted: {type: Boolean, default: false},

    societyInactive: {type: Boolean, default: false},

});



// Save user's hashed password
UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function () {

            }, function (err, hash) {
                if (err) {
                    return next(err);
                }
                // saving actual password as hash
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

// compare two password

UserSchema.methods.comparePassword = function (pw, cb) {
    bcrypt.compare(pw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

UserSchema.statics.getEmailListForSociety =  function (societyId, cb) {
  var userModel = this;
  userModel
  .find({role: "Resident", societyId: societyId, softDeleted: {$ne: true}, status: 'active'})
  .select("contactEmail")
  .exec(function (err, users) {
    if (err) {
      cb(err);
    } else {
      var emailList = users.reduce(function (acc, cur) {
        return acc + cur.contactEmail + ', ';
      }, '')
      cb(null, emailList.slice(0,-2)); // remove the trailing , and space
    }
  })
}
UserSchema.statics.getEmailListForFlat =  function (societyId, flatNo, block, cb) {
  var userModel = this;
  userModel
  .find({role: "Resident", societyId: societyId, flatNo: flatNo, block: block, softDeleted: {$ne: true}, status: 'active'})
  .select("contactEmail")
  .exec(function (err, users) {
    if (err) {
      cb(err);
    } else {
      var emailList = users.reduce(function (acc, cur) {
        return acc + cur.contactEmail + ',';
      }, '')
      cb(null, emailList.slice(0,-1)); // remove the trailing ,
    }
  })
}

UserSchema.virtual('awsProfilePicURL').get(function () {
  return 'https://'+awsEndpoint+awsBucket+'/' + this._id;
})

UserSchema.set('toJSON', {virtuals: true});
UserSchema.set('toObject', {virtuals: true});

module.exports = mongoose.model('User', UserSchema);
