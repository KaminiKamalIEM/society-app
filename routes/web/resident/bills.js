var User = require('../../../models/user');
var Bill = require('../../../models/bill');

var moment = require('moment');

module.exports = {

  get: function(req, res){
    var block = req.session.user.block;
    var flatNo = req.session.user.flatNo;
    Bill
    .find({block: block, flatNo: flatNo})
    .exec()
    .then(function (bills) {
      return res.render("resident/bills", {
        title: "My Bills",
        bills: bills,
        moment: moment
      })
    })

  },


};
