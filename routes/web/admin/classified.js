var User = require('../../../models/user');
var Classified = require('../../../models/classified');

var config = require('../../../config/main')[process.env.NODE_ENV || 'development'];

var categoryList = require("../../../config/types")['categoryList'];
var moment =require("moment");

module.exports = {


  get: function(req, res) {
    var adminId = req.session.user.id;
    var societyId = req.session.user.societyId;

    var filter = {"societyId": societyId};
    var limit = 20; // If NO filters, show just 20

    if (req.query.isOffering) {
      filter.isOffering = (req.query.isOffering == "0") ? false: true;
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.category) {
      filter.category = req.query.category;
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.minprice) {
      filter.price = {$gte: Number(req.query.minprice)};
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.maxprice) {
      filter.price = {$lte: Number(req.query.maxprice)};
      limit = null; // If a filter is active, there's no limit!
    }

    console.log((filter));

    Classified.find(filter)
      .limit(limit)
      .populate("addedBy", "name")
      .exec(function(err, ad) {
        if (err) {
          console.log(err);
          return res.json({
            error: true,
            message: "Could not get anything",
          });
        } else {
          res.render('admin/classified', {
            error: false,
            moment: moment,
            title: 'Classified Ads',
            data: ad,
            categoryList: categoryList.map(function (string) {return string.charAt(0).toUpperCase() + string.slice(1);}), // capitalize first letter
            query: (req.query === undefined || req.query === null) ? {} : req.query
          })
          // res.json({
          //   error: false,
          //   title: 'Classified Ads',
          //   data: ad,
          //   categoryList: categoryList
          // })
        }
    });
  },

  put: function (req, res) {
    var adId = req.params.adId;
    Classified.update(
      {_id: adId}, {approvalStatus: req.body.approvalStatus}, {}, function (err, numAffected) {
        if (err || numAffected == 0) {
          console.log(err);
          return res.json({
            error: true,
            message: "Couldn't update!"
          });
        }
        res.json({
          error: false
        });

      }
    );
  },

  putStopDate: function (req, res) {
    var adId = req.params.adId;
    console.log(req.body.stopDate, adId);
    var stopDateMoment = moment(req.body.stopDate, 'DD/MM/YYYY HH:mm');
    if (!stopDateMoment.isValid()) {
      return res.json({error: true, message: "Invalid Stop Date!"});
    }
    Classified.update(
      {_id: adId}, {stopDate: stopDateMoment.toDate()}, {}, function (err, numAffected) {
        if (err || numAffected == 0) {
          console.log(err);
          return res.json({
            error: true,
            message: "Couldn't update!"
          });
        }
        res.json({
          error: false
        });

      }
    );
  },

  // 09/01/2017
  putMyAd: function(req, res) {
    var adminId = req.session.user.id;
    var societyId = req.session.user.societyId;
    var adId = req.params.adId;
    var data = req.body;
    data.approvalStatus = "Approved";

    data.from = moment(req.body.from, 'DD/MM/YYYY HH:mm').toDate();
    data.validTill = moment(req.body.validTill, 'DD/MM/YYYY HH:mm').toDate();

    Classified.update({
      _id: adId,
      addedBy: adminId
    }, data, {}, function(err, numAffected) {
      if (err || numAffected == 0) {
        console.log(err);
        return res.json({
          error: true,
          message: "Couldn't update!"
        });
      }
      res.json({
        error: false,
        data: {
          id: adId
        }
      });

    });

  },

  getNew: function(req, res) {
    Classified.find().exec(function(err, ads) {
      if (err) {
        res.render('admin/classified_add', {
          title: 'Add a Classified',
          categoryList: []
        });
      }
      // var categoryList = ads.map(function(i) {
      //   return i.category;
      // });
      // categoryList = categoryList.filter(function(item, pos) {
      //   return categoryList.indexOf(item) == pos; // prune duplicate entries
      // });
      res.render('admin/classified_add', {
        title: 'Add a Classified',
        categoryList: categoryList.map(function(string) {
          return string.charAt(0).toUpperCase() + string.slice(1);
        }),
        tempCreds: res.locals.tempCreds,
        awsRegion: config.aws.region,
        awsImageBucketName: config.aws.imageBucketName
      });
    })
  },

  getEdit: function(req, res) {
    var adId = req.params.adId;
    Classified.findOne({
      _id: adId
    }, function(err, ad) {
      if (err) {
        res.json({
          error: true,
          reason: err
        });
      }
      console.log(adId, ad);
      // var categoryList = ads.map(function(i) {
      //   return i.category;
      // });
      // categoryList = categoryList.filter(function(item, pos) {
      //   return categoryList.indexOf(item) == pos; // prune duplicate entries
      // });
      res.render('admin/classified_edit', {
        title: 'Edit Ad',
        adId: adId,
        data: ad,
        categoryList: categoryList.map(function(string) {
          return string.charAt(0).toUpperCase() + string.slice(1);
        }),
        tempCreds: res.locals.tempCreds,
        awsRegion: config.aws.region,
        awsImageBucketName: config.aws.imageBucketName,
        moment: moment
      });
    })
  },


    getMyAds: function(req, res) {
      var adminId = req.session.user.id;
      var societyId = req.session.user.societyId;

      Classified.find({
        societyId: societyId,
        addedBy: adminId
      }).exec(function(err, ads) {
        if (err) {
          console.log(err);
          return res.json({
            error: true,
            message: "Could not get anything",
          });
        } else {
          res.render('admin/classified_myads', {
              error: false,
              moment: moment,
              title: 'My Ads',
              data: ads,
            })
            // res.json({
            //   error: false,
            //   title: 'My Ads',
            //   data: ads
            // })
        }
      })
    },


      postNew: function(req, res) {
        console.log(req.body);
        // console.log(new ObjectId("5799ae6eb3b21ea20ff4eaf5"));
        var data = req.body;
        data.addedBy = req.session.user.id;
        data.societyId = req.session.user.societyId;

        data.approvalStatus =  "Approved";

        data.from = moment(req.body.from, 'DD/MM/YYYY HH:mm').toDate();
        data.validTill = moment(req.body.validTill, 'DD/MM/YYYY HH:mm').toDate();

        var classified = new Classified(data);
        classified.save(function(err) {
          if (err) {
            console.log(err);
            return res.json({
              error: true,
              message: "Fill the mandatory fields"
            });
          } else {
            res.json({
              error: false,
              data: {
                id: classified.id
              }
            });
          }
        });
      },


      del: function(req, res) {
        var adId = req.params.adId;
        var adminId = req.session.user.id;

        Classified.remove({
          _id: adId,
          addedBy: adminId
        }, function(err) {
          if (err) {
            res.json({
              error: true,
              message: err
            })
          }
          res.json({
            error: false
          });
        })
      }

}
